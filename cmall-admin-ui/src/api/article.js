import request from '@/utils/request'

export function listArticles(query) {
  return request({
    url: '/article/list',
    method: 'get',
    params: query
  })
}

export function publishArticle(data) {
  return request({
    url: '/article/publish',
    method: 'post',
    data
  })
}

