package com.cc.cmall.api.auth.modular.mapper;

import com.cc.cmall.api.auth.model.entity.CmallCollect;
import com.cc.cmall.api.auth.model.entity.CmallCollectExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CmallCollectMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cmall_collect
     *
     * @mbggenerated Thu Oct 11 14:44:04 CST 2018
     */
    int countByExample(CmallCollectExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cmall_collect
     *
     * @mbggenerated Thu Oct 11 14:44:04 CST 2018
     */
    int deleteByExample(CmallCollectExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cmall_collect
     *
     * @mbggenerated Thu Oct 11 14:44:04 CST 2018
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cmall_collect
     *
     * @mbggenerated Thu Oct 11 14:44:04 CST 2018
     */
    int insert(CmallCollect record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cmall_collect
     *
     * @mbggenerated Thu Oct 11 14:44:04 CST 2018
     */
    int insertSelective(CmallCollect record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cmall_collect
     *
     * @mbggenerated Thu Oct 11 14:44:04 CST 2018
     */
    List<CmallCollect> selectByExample(CmallCollectExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cmall_collect
     *
     * @mbggenerated Thu Oct 11 14:44:04 CST 2018
     */
    CmallCollect selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cmall_collect
     *
     * @mbggenerated Thu Oct 11 14:44:04 CST 2018
     */
    int updateByExampleSelective(@Param("record") CmallCollect record, @Param("example") CmallCollectExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cmall_collect
     *
     * @mbggenerated Thu Oct 11 14:44:04 CST 2018
     */
    int updateByExample(@Param("record") CmallCollect record, @Param("example") CmallCollectExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cmall_collect
     *
     * @mbggenerated Thu Oct 11 14:44:04 CST 2018
     */
    int updateByPrimaryKeySelective(CmallCollect record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cmall_collect
     *
     * @mbggenerated Thu Oct 11 14:44:04 CST 2018
     */
    int updateByPrimaryKey(CmallCollect record);
}