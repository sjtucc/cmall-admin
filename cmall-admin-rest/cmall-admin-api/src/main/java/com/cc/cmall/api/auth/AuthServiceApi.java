package com.cc.cmall.api.auth;

import com.cc.cmall.api.auth.model.entity.CmallAdmin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Set;

/**
 * 用户中心接口
 *
 * @author chenchen
 * @date 2017年11月09日19:38:34
 */
@RequestMapping("/api/authService")
public interface AuthServiceApi {

    /**
     * 根据用户id获取用户信息
     *
     * @author chenchen
     * @Date 2017年11月09日19:38:51
     */
    @RequestMapping("/getUserById")
    CmallAdmin getUserById(@RequestParam("userId") Long userId);

    /**
     * 根据用户name获取用户信息
     *
     * @author chenchen
     * @Date 2017年11月09日19:38:51
     */
    @RequestMapping("/getUserByName")
    CmallAdmin getUserByName(@RequestParam("username") String username);

    /**
     * 获取用户的权限路径
     *
     * @author chenchen
     * @Date 2017/11/14 上午11:41
     */
    @RequestMapping("/getUserPermissionUrls")
    Set<String> getUserPermissionUrls(@RequestParam("userId") Long userId);

    /**
     * 通过appId获取secret
     *
     * @author chenchen
     * @Date 2018/5/12 23:20
     */
    @RequestMapping("/getSecretByAppId")
    String getSecretByAppId(@RequestParam("appId") String appId);
}
