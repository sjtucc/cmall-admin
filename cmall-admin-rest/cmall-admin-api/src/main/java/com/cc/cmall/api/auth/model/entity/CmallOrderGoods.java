package com.cc.cmall.api.auth.model.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class CmallOrderGoods {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_order_goods.id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_order_goods.order_id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private Integer orderId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_order_goods.goods_id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private Integer goodsId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_order_goods.goods_name
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private String goodsName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_order_goods.goods_sn
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private String goodsSn;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_order_goods.product_id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private Integer productId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_order_goods.number
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private Short number;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_order_goods.price
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private BigDecimal price;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_order_goods.specifications
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private String[] specifications;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_order_goods.pic_url
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private String picUrl;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_order_goods.add_time
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private LocalDateTime addTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_order_goods.deleted
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private Boolean deleted;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_order_goods.version
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private Integer version;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_order_goods.id
     *
     * @return the value of cmall_order_goods.id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_order_goods.id
     *
     * @param id the value for cmall_order_goods.id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_order_goods.order_id
     *
     * @return the value of cmall_order_goods.order_id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_order_goods.order_id
     *
     * @param orderId the value for cmall_order_goods.order_id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_order_goods.goods_id
     *
     * @return the value of cmall_order_goods.goods_id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public Integer getGoodsId() {
        return goodsId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_order_goods.goods_id
     *
     * @param goodsId the value for cmall_order_goods.goods_id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_order_goods.goods_name
     *
     * @return the value of cmall_order_goods.goods_name
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public String getGoodsName() {
        return goodsName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_order_goods.goods_name
     *
     * @param goodsName the value for cmall_order_goods.goods_name
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_order_goods.goods_sn
     *
     * @return the value of cmall_order_goods.goods_sn
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public String getGoodsSn() {
        return goodsSn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_order_goods.goods_sn
     *
     * @param goodsSn the value for cmall_order_goods.goods_sn
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setGoodsSn(String goodsSn) {
        this.goodsSn = goodsSn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_order_goods.product_id
     *
     * @return the value of cmall_order_goods.product_id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public Integer getProductId() {
        return productId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_order_goods.product_id
     *
     * @param productId the value for cmall_order_goods.product_id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_order_goods.number
     *
     * @return the value of cmall_order_goods.number
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public Short getNumber() {
        return number;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_order_goods.number
     *
     * @param number the value for cmall_order_goods.number
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setNumber(Short number) {
        this.number = number;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_order_goods.price
     *
     * @return the value of cmall_order_goods.price
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_order_goods.price
     *
     * @param price the value for cmall_order_goods.price
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_order_goods.specifications
     *
     * @return the value of cmall_order_goods.specifications
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public String[] getSpecifications() {
        return specifications;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_order_goods.specifications
     *
     * @param specifications the value for cmall_order_goods.specifications
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setSpecifications(String[] specifications) {
        this.specifications = specifications;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_order_goods.pic_url
     *
     * @return the value of cmall_order_goods.pic_url
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_order_goods.pic_url
     *
     * @param picUrl the value for cmall_order_goods.pic_url
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_order_goods.add_time
     *
     * @return the value of cmall_order_goods.add_time
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_order_goods.add_time
     *
     * @param addTime the value for cmall_order_goods.add_time
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_order_goods.deleted
     *
     * @return the value of cmall_order_goods.deleted
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_order_goods.deleted
     *
     * @param deleted the value for cmall_order_goods.deleted
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_order_goods.version
     *
     * @return the value of cmall_order_goods.version
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_order_goods.version
     *
     * @param version the value for cmall_order_goods.version
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cmall_order_goods
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", orderId=").append(orderId);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", goodsName=").append(goodsName);
        sb.append(", goodsSn=").append(goodsSn);
        sb.append(", productId=").append(productId);
        sb.append(", number=").append(number);
        sb.append(", price=").append(price);
        sb.append(", specifications=").append(specifications);
        sb.append(", picUrl=").append(picUrl);
        sb.append(", addTime=").append(addTime);
        sb.append(", deleted=").append(deleted);
        sb.append(", version=").append(version);
        sb.append("]");
        return sb.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cmall_order_goods
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        CmallOrderGoods other = (CmallOrderGoods) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getOrderId() == null ? other.getOrderId() == null : this.getOrderId().equals(other.getOrderId()))
            && (this.getGoodsId() == null ? other.getGoodsId() == null : this.getGoodsId().equals(other.getGoodsId()))
            && (this.getGoodsName() == null ? other.getGoodsName() == null : this.getGoodsName().equals(other.getGoodsName()))
            && (this.getGoodsSn() == null ? other.getGoodsSn() == null : this.getGoodsSn().equals(other.getGoodsSn()))
            && (this.getProductId() == null ? other.getProductId() == null : this.getProductId().equals(other.getProductId()))
            && (this.getNumber() == null ? other.getNumber() == null : this.getNumber().equals(other.getNumber()))
            && (this.getPrice() == null ? other.getPrice() == null : this.getPrice().equals(other.getPrice()))
            && (this.getSpecifications() == null ? other.getSpecifications() == null : this.getSpecifications().equals(other.getSpecifications()))
            && (this.getPicUrl() == null ? other.getPicUrl() == null : this.getPicUrl().equals(other.getPicUrl()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()));
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cmall_order_goods
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getOrderId() == null) ? 0 : getOrderId().hashCode());
        result = prime * result + ((getGoodsId() == null) ? 0 : getGoodsId().hashCode());
        result = prime * result + ((getGoodsName() == null) ? 0 : getGoodsName().hashCode());
        result = prime * result + ((getGoodsSn() == null) ? 0 : getGoodsSn().hashCode());
        result = prime * result + ((getProductId() == null) ? 0 : getProductId().hashCode());
        result = prime * result + ((getNumber() == null) ? 0 : getNumber().hashCode());
        result = prime * result + ((getPrice() == null) ? 0 : getPrice().hashCode());
        result = prime * result + ((getSpecifications() == null) ? 0 : getSpecifications().hashCode());
        result = prime * result + ((getPicUrl() == null) ? 0 : getPicUrl().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        return result;
    }
}