package com.cc.cmall.api.auth.model.entity;

import java.time.LocalDateTime;

public class CmallComment {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_comment.id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_comment.value_id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private Integer valueId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_comment.type
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private Byte type;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_comment.content
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private String content;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_comment.user_id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private Integer userId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_comment.has_picture
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private Boolean hasPicture;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_comment.pic_urls
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private String[] picUrls;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_comment.star
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private Short star;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_comment.add_time
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private LocalDateTime addTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_comment.deleted
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private Boolean deleted;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column cmall_comment.version
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    private Integer version;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_comment.id
     *
     * @return the value of cmall_comment.id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_comment.id
     *
     * @param id the value for cmall_comment.id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_comment.value_id
     *
     * @return the value of cmall_comment.value_id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public Integer getValueId() {
        return valueId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_comment.value_id
     *
     * @param valueId the value for cmall_comment.value_id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setValueId(Integer valueId) {
        this.valueId = valueId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_comment.type
     *
     * @return the value of cmall_comment.type
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public Byte getType() {
        return type;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_comment.type
     *
     * @param type the value for cmall_comment.type
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setType(Byte type) {
        this.type = type;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_comment.content
     *
     * @return the value of cmall_comment.content
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public String getContent() {
        return content;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_comment.content
     *
     * @param content the value for cmall_comment.content
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_comment.user_id
     *
     * @return the value of cmall_comment.user_id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_comment.user_id
     *
     * @param userId the value for cmall_comment.user_id
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_comment.has_picture
     *
     * @return the value of cmall_comment.has_picture
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public Boolean getHasPicture() {
        return hasPicture;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_comment.has_picture
     *
     * @param hasPicture the value for cmall_comment.has_picture
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setHasPicture(Boolean hasPicture) {
        this.hasPicture = hasPicture;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_comment.pic_urls
     *
     * @return the value of cmall_comment.pic_urls
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public String[] getPicUrls() {
        return picUrls;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_comment.pic_urls
     *
     * @param picUrls the value for cmall_comment.pic_urls
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setPicUrls(String[] picUrls) {
        this.picUrls = picUrls;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_comment.star
     *
     * @return the value of cmall_comment.star
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public Short getStar() {
        return star;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_comment.star
     *
     * @param star the value for cmall_comment.star
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setStar(Short star) {
        this.star = star;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_comment.add_time
     *
     * @return the value of cmall_comment.add_time
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_comment.add_time
     *
     * @param addTime the value for cmall_comment.add_time
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_comment.deleted
     *
     * @return the value of cmall_comment.deleted
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_comment.deleted
     *
     * @param deleted the value for cmall_comment.deleted
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column cmall_comment.version
     *
     * @return the value of cmall_comment.version
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column cmall_comment.version
     *
     * @param version the value for cmall_comment.version
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cmall_comment
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", valueId=").append(valueId);
        sb.append(", type=").append(type);
        sb.append(", content=").append(content);
        sb.append(", userId=").append(userId);
        sb.append(", hasPicture=").append(hasPicture);
        sb.append(", picUrls=").append(picUrls);
        sb.append(", star=").append(star);
        sb.append(", addTime=").append(addTime);
        sb.append(", deleted=").append(deleted);
        sb.append(", version=").append(version);
        sb.append("]");
        return sb.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cmall_comment
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        CmallComment other = (CmallComment) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getValueId() == null ? other.getValueId() == null : this.getValueId().equals(other.getValueId()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getContent() == null ? other.getContent() == null : this.getContent().equals(other.getContent()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getHasPicture() == null ? other.getHasPicture() == null : this.getHasPicture().equals(other.getHasPicture()))
            && (this.getPicUrls() == null ? other.getPicUrls() == null : this.getPicUrls().equals(other.getPicUrls()))
            && (this.getStar() == null ? other.getStar() == null : this.getStar().equals(other.getStar()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()));
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table cmall_comment
     *
     * @mbggenerated Thu Oct 11 14:37:12 CST 2018
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getValueId() == null) ? 0 : getValueId().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getContent() == null) ? 0 : getContent().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getHasPicture() == null) ? 0 : getHasPicture().hashCode());
        result = prime * result + ((getPicUrls() == null) ? 0 : getPicUrls().hashCode());
        result = prime * result + ((getStar() == null) ? 0 : getStar().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        return result;
    }
}