package com.cc.cmall.api.common.constants;

/**
 * 框架通用常量
 *
 * @author chenchen
 * @date 2018-05-11 23:46
 */
public interface CMallConstants {

    /**
     * 请求号header标识
     */
    String REQUEST_NO_HEADER_NAME = "Request-No";

    /**
     * header中的spanId，传递规则：request header中传递本服务的id
     */
    String SPAN_ID_HEADER_NAME = "Span-Id";
}
