package com.cc.cmall.core.config;

import com.cc.cmall.core.utils.SpringContextHolder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 默认的工具类
 *
 * @author chenchen
 * @date 2018-01-07 12:33
 */
@Configuration
public class DefaultUtilConfig {

    @Bean
    public SpringContextHolder springContextHolder() {
        return new SpringContextHolder();
    }
}
