package com.cc.cmall.core.config.properties;

/**
 * 应用名称属性
 *
 * @author chenchen
 * @Date 2018/5/8 下午2:24
 */
public class AppNameProperties {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
