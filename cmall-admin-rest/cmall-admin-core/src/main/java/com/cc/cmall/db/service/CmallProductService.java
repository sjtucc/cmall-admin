package com.cc.cmall.db.service;

import com.cc.cmall.api.auth.model.entity.CmallProduct;
import com.cc.cmall.api.auth.model.entity.CmallProductExample;
import com.cc.cmall.api.auth.modular.mapper.CmallProductMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CmallProductService {
    @Resource
    private CmallProductMapper productMapper;

    public List<CmallProduct> queryByGid(Integer gid) {
        CmallProductExample example = new CmallProductExample();
        example.or().andGoodsIdEqualTo(gid).andDeletedEqualTo(false);
        return productMapper.selectByExample(example);
    }

    public CmallProduct findById(Integer id) {
        return productMapper.selectByPrimaryKey(id);
    }

    public int updateById(CmallProduct product) {
        return productMapper.updateWithVersionByPrimaryKeySelective(product.getVersion(), product);
    }

    public void deleteById(Integer id) {
        productMapper.logicalDeleteByPrimaryKey(id);
    }

    public void add(CmallProduct product) {
        productMapper.insertSelective(product);
    }

    public int count() {
        CmallProductExample example = new CmallProductExample();
        example.or().andDeletedEqualTo(false);

        return (int)productMapper.countByExample(example);
    }

    public void deleteByGid(Integer gid) {
        CmallProductExample example = new CmallProductExample();
        example.or().andGoodsIdEqualTo(gid);
        productMapper.logicalDeleteByExample(example);
    }
}
