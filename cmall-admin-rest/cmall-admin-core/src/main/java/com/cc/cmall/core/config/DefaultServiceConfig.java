package com.cc.cmall.core.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @author chenchen
 * @date 2018/10/26 11:11
 */
@Configuration
@ComponentScan(basePackages = {"com.cc.cmall.core","com.cc.cmall.db"})
public class DefaultServiceConfig {

}
