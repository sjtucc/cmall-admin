package com.cc.cmall.core.consumer;

import com.cc.cmall.api.auth.AuthServiceApi;
import com.cc.cmall.core.constant.Constant;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 用户中心服务
 *
 * @author chenchen
 * @date 2017-11-09-下午7:52
 */
@FeignClient(Constant.AUTH_MODULAR_NAME)
public interface AuthServiceConsumer extends AuthServiceApi {

}