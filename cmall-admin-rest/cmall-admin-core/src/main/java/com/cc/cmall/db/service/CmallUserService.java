package com.cc.cmall.db.service;

import com.baomidou.mybatisplus.plugins.pagination.PageHelper;
import com.cc.cmall.api.auth.model.entity.CmallUser;
import com.cc.cmall.api.auth.model.entity.CmallUserExample;
import com.cc.cmall.api.auth.modular.mapper.CmallUserMapper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import javax.annotation.Resource;
import java.util.List;

@Service
public class CmallUserService {
    @Resource
    private CmallUserMapper userMapper;

    public CmallUser findById(Integer userId) {
        return userMapper.selectByPrimaryKey(userId);
    }

    public CmallUser queryByOid(String openId) {
        CmallUserExample example = new CmallUserExample();
        example.or().andWeixinOpenidEqualTo(openId).andDeletedEqualTo(false);
        return userMapper.selectOneByExample(example);
    }

    public void add(CmallUser user) {
        userMapper.insertSelective(user);
    }

    public int update(CmallUser user) {
        return userMapper.updateWithVersionByPrimaryKeySelective(user.getVersion(), user);
    }

    public List<CmallUser> querySelective(String username, String mobile, Integer page, Integer size, String sort, String order) {
        CmallUserExample example = new CmallUserExample();
        CmallUserExample.Criteria criteria = example.createCriteria();

        if(!StringUtils.isEmpty(username)){
            criteria.andUsernameLike("%" + username + "%");
        }
        if(!StringUtils.isEmpty(mobile)){
            criteria.andMobileEqualTo(mobile);
        }
        criteria.andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, size);
        return userMapper.selectByExample(example);
    }

    public int countSeletive(String username, String mobile, Integer page, Integer size, String sort, String order) {
        CmallUserExample example = new CmallUserExample();
        CmallUserExample.Criteria criteria = example.createCriteria();

        if(!StringUtils.isEmpty(username)){
            criteria.andUsernameLike("%" + username + "%");
        }
        if(!StringUtils.isEmpty(mobile)){
            criteria.andMobileEqualTo(mobile);
        }
        criteria.andDeletedEqualTo(false);

        return (int) userMapper.countByExample(example);
    }

    public int count() {
        CmallUserExample example = new CmallUserExample();
        example.or().andDeletedEqualTo(false);

        return (int)userMapper.countByExample(example);
    }

    public List<CmallUser> queryByUsername(String username) {
        CmallUserExample example = new CmallUserExample();
        example.or().andUsernameEqualTo(username).andDeletedEqualTo(false);
        return userMapper.selectByExample(example);
    }

    public List<CmallUser> queryByMobile(String mobile) {
        CmallUserExample example = new CmallUserExample();
        example.or().andMobileEqualTo(mobile).andDeletedEqualTo(false);
        return userMapper.selectByExample(example);
    }

    public void deleteById(Integer id) {
        userMapper.logicalDeleteByPrimaryKey(id);
    }
}
