package com.cc.cmall.core.base.controller;

import cn.hutool.core.bean.BeanUtil;
import com.cc.cmall.api.common.exception.CoreExceptionEnum;
import com.cc.cmall.core.base.response.ResponseData;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

/**
 * 重写spring默认响应提示信息
 *
 * @author chenchen
 * @date 2017-11-14-下午5:54
 */
public class DefaultCMallErrorAttributes extends DefaultErrorAttributes {

    @Override
    public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {
        return BeanUtil.beanToMap(ResponseData.error(CoreExceptionEnum.SERVICE_ERROR.getMessage()));

    }
}
