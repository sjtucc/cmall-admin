package com.cc.cmall.core.config;

import com.cc.cmall.core.content.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 默认auth远程调用配置
 *
 * @author chenchen
 * @date 2018-01-11 21:29
 */
@Configuration
public class DefaultAuthConfig {

    /**
     * 调用当前登录用户的工具类
     */
    @Bean
    public LoginContext loginContext() {
        return new LoginContext();
    }
}
