package com.cc.cmall.core.config;

import com.cc.cmall.core.exception.DefualtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 *
 * @author chenchen
 * @date 2016年11月12日 下午4:48:10
 */
@Configuration
@EnableAspectJAutoProxy
public class DefaultAopConfig {

    /**
     * 默认的异常拦截器
     */
    @Bean
    public DefualtExceptionHandler defaultControllerExceptionHandler() {
        return new DefualtExceptionHandler();
    }

}