package com.cc.cmall.register;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @Description:
 * @author chenchen
 * @date 2018/9/29 16:52
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableEurekaServer
public class CMallRegisterApplication {
    static Logger logger = LoggerFactory.getLogger(CMallRegisterApplication.class);
    public static void main(String[] args) {
        SpringApplication.run(CMallRegisterApplication.class, args);
    }

}
