package com.cc.cmall.gate;

import com.cc.cmall.core.consumer.AuthServiceConsumer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Description:
 * @author chenchen
 * @date 2018/9/30 9:55
 */

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableFeignClients(clients = {AuthServiceConsumer.class})
@EnableZuulProxy
@EnableEurekaClient
@ComponentScan(basePackages = {"com.cc.cmall.gate","com.cc.cmall.core","com.cc.cmall.db"})
public class CMallGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(CMallGatewayApplication.class, args);
    }
}
