package com.cc.cmall.gate.controller;

import com.cc.cmall.core.base.response.ResponseUtil;
import com.cc.cmall.db.service.CmallGoodsService;
import com.cc.cmall.db.service.CmallOrderService;
import com.cc.cmall.db.service.CmallProductService;
import com.cc.cmall.db.service.CmallUserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @author chenchen
 * @date 2018/10/11 14:05
 */

@RestController
@RequestMapping("/gate/dashboard")
public class DashbordController {
    private final Log logger = LogFactory.getLog(DashbordController.class);

    @Autowired
    private CmallUserService userService;
    @Autowired
    private CmallGoodsService goodsService;
    @Autowired
    private CmallProductService productService;
    @Autowired
    private CmallOrderService orderService;

    @GetMapping("")
    public Object info(){

        int userTotal = userService.count();
        int goodsTotal = goodsService.count();
        int productTotal = productService.count();
        int orderTotal = orderService.count();
        Map<String, Integer> data = new HashMap<>();
        data.put("userTotal", userTotal);
        data.put("goodsTotal", goodsTotal);
        data.put("productTotal", productTotal);
        data.put("orderTotal", orderTotal);

        return ResponseUtil.ok(data);
    }
}
