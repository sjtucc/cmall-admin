package com.cc.cmall.gate.filter;

import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.cc.cmall.api.common.constants.CMallConstants;
import com.cc.cmall.gate.constants.ZuulConstants;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

import javax.servlet.http.HttpServletResponse;

/**
 * requestNo生成过滤器
 *
 * @author chenchen
 * @date 2017-11-08-下午2:49
 */
public class RequestNoGenerateFilter extends ZuulFilter {

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return ZuulConstants.REQUEST_NO_GENERATE_FILTER_ORDER;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext currentContext = RequestContext.getCurrentContext();
        HttpServletResponse response = currentContext.getResponse();

        String requestNo = IdWorker.getIdStr();

        currentContext.addZuulRequestHeader(CMallConstants.REQUEST_NO_HEADER_NAME, requestNo);

        response.addHeader(CMallConstants.REQUEST_NO_HEADER_NAME, requestNo);

        return null;
    }
}
