package com.cc.cmall.gate.controller;

import com.cc.cmall.api.auth.model.entity.CmallAdmin;
import com.cc.cmall.core.base.response.ResponseUtil;
import com.cc.cmall.core.consumer.AuthServiceConsumer;
import com.cc.cmall.core.utils.SpringContextHolder;
import com.cc.cmall.gate.utils.AdminTokenManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @author chenchen
 * @date 2018/10/22 16:05
 */
@RestController
@RequestMapping("/gate/admin")
public class AdminController {
    @GetMapping("/info")
    public Object info(String token){
        Integer adminId = AdminTokenManager.getUserId(token);
        if(adminId == null){
            return ResponseUtil.badArgumentValue();
        }
        AuthServiceConsumer authServiceConsumer = SpringContextHolder.getBean(AuthServiceConsumer.class);
        CmallAdmin admin = authServiceConsumer.getUserById(Long.valueOf(1));
        if(admin == null){
            return ResponseUtil.badArgumentValue();
        }

        Map<String, Object> data = new HashMap<>();
        data.put("name", admin.getUsername());
        data.put("avatar", admin.getAvatar());

        // 目前roles不支持，这里简单设置admin
        List<String> roles = new ArrayList<>();
        roles.add("admin");
        data.put("roles", roles);
        data.put("introduction", "admin introduction");
        return ResponseUtil.ok(data);
    }
}
