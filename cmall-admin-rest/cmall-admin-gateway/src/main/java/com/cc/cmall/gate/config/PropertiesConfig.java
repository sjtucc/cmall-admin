package com.cc.cmall.gate.config;

import com.cc.cmall.core.config.properties.SignProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置文件获取
 *
 * @author chenchen
 * @date 2018-05-08-下午3:57
 */
@Configuration
public class PropertiesConfig {

    @Bean
    @ConfigurationProperties(prefix = "cmall.sign")
    public SignProperties signProperties() {
        return new SignProperties();
    }

}
