package com.cc.cmall.gate.controller;

import com.cc.cmall.core.base.response.ResponseUtil;
import com.cc.cmall.gate.utils.AdminToken;
import com.cc.cmall.gate.utils.AdminTokenManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录控制器
 *
 * @author chenchen
 * @date 2017-11-08-下午7:04
 */
@RestController
@RequestMapping("/gate/login")
public class LoginController {

    @RequestMapping("/login")
    public Object login() {
        AdminToken adminToken = AdminTokenManager.generateToken(1);
        return ResponseUtil.ok(adminToken.getToken());
    }

    @RequestMapping("/logout")
    public Object logout() {
        return null;
    }

    @RequestMapping("/checkToken")
    public Object checkToken() {
         return "check";
    }
}
