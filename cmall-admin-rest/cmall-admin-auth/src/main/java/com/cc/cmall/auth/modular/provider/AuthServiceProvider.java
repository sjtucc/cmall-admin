package com.cc.cmall.auth.modular.provider;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.cc.cmall.api.auth.AuthServiceApi;
import com.cc.cmall.api.auth.model.entity.CmallAdmin;
import com.cc.cmall.api.auth.modular.mapper.CmallAdminMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Set;

/**
 * auth接口实现
 *
 * @author chenchen
 * @date 2017-11-09-下午7:47
 */
@RestController
public class AuthServiceProvider implements AuthServiceApi {

    @Autowired
    private CmallAdminMapper cmallAdminMapper;

    @Override
    public CmallAdmin getUserById(Long userId) {
       return cmallAdminMapper.selectById(userId);
    }

    @Override
    public CmallAdmin getUserByName(String username) {
        List<CmallAdmin> admins = cmallAdminMapper.selectList(
                new EntityWrapper<CmallAdmin>().eq("username", username));
        if(admins != null && !admins.isEmpty()) {
            return admins.get(0);
        } else {
            return null;
        }
    }

    @Override
    public Set<String> getUserPermissionUrls(Long userId) {
        return null;
    }

    @Override
    public String getSecretByAppId(String appId) {
        return null;
    }
}
