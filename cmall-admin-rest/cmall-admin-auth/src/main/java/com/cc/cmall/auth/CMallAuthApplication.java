package com.cc.cmall.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 认证中心服务
 *
 * @author chenchen
 * @Date 2018/1/22 21:27
 */
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
public class CMallAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(CMallAuthApplication.class, args);
    }

}
