package com.cc.cmall.auth.config;

import com.cc.cmall.core.base.controller.DefaultCMallErrorAttributes;
import com.cc.cmall.core.feign.CMallFeignErrorDecoder;
import com.cc.cmall.core.feign.CMallFeignHeaderProcessInterceptor;
import feign.Feign;
import feign.RequestInterceptor;
import feign.hystrix.HystrixFeign;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * feign的错误编码配置（为了feign接收到错误的返回，转化成可识别的ServiceException）
 *
 * @author chenchen
 * @Date 2018/4/20 23:11
 */
@Configuration
public class FeignConfig {

    /**
     * 自定义错误解码器
     */
    @Bean
    @Scope("prototype")
    public Feign.Builder feignHystrixBuilder() {
        return HystrixFeign.builder().errorDecoder(new CMallFeignErrorDecoder());
    }

    /**
     * feign请求加上当前请求接口的headers
     */
    @Bean
    public RequestInterceptor requestInterceptor() {
        return new CMallFeignHeaderProcessInterceptor();
    }

    /**
     * 覆盖spring默认的响应消息格式
     */
    @Bean
    public DefaultCMallErrorAttributes defaultCMallErrorAttributes() {
        return new DefaultCMallErrorAttributes();
    }

}